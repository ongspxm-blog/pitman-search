import sys

def search(searchword):
    done = False
    dictname, dicts = 'dict.txt', []
    with open(dictname, 'r') as dictfile:
        for w in dictfile.read().split('\n'):
            if not w: continue
            word, page = w.split(' ')[0], int(w.split(' ')[1])

            if word < searchword:
                lastword = word
            else:
                done = True
                break
        if searchword > word:
            page += 1
            lastword, word = word, ''
        print(f'check around pg {page}')
        print(f'words: {lastword}, {searchword}, {word}')

if __name__ == '__main__':
    if len(sys.argv) > 1:
        search(sys.argv[1])
    else:
        print('ERR: search.py <word>')
