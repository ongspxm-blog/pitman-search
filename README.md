# pitman dictionary search
There is not great pitman shorthand dictionary online, There is a possible pitman shorthand converter, but then again, its online, and there are usage limits. So here, we bring the analoge solution to you.

Shorthand dictionaries is the classical way people learn shorthand, so you are sure that there is going to be sufficient information for us to work with, but the problem here lies in the search.

This projects is built upon the pdf scan of a pitman shorthand dictionary, and basically preprocess the words so that you can easy find which page to look for. The system is not perfect, but the idea is there.

So you search up the word, this system guesses the page for you, and you can visit it in your favourite pdf viewer.

`USAGE: python search.py <searchword>`

Most of the code I used to made this is available in the repo, hope this helps.

- [online pitman](https://steno.tu-clausthal.de/Pitman.php), can check it out
- [source of pitman dictionary](https://archive.org/details/in.ernet.dli.2015.449114/page/n775/mode/2up?q=waiting)

