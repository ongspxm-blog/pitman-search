import re

import PyPDF2

testsize = 5 
pg0, pg1 = 57, 796

pdfname = 'web/pitman_dict.pdf'
dictwords = [('', pg0-1)]

with open(pdfname, 'rb') as pdfobj:
    read_pdf = PyPDF2.PdfFileReader(pdfobj)

    for pg_extra in range(pg1-pg0):
        pg = read_pdf.getPage(pg0+pg_extra)
        words = [w.lower() for w in re.split('[^a-zA-Z]', pg.extractText()) if len(w)>4]

        testleng = 0 
        while words:
            chars = {}
            testleng += 1

            for w in words:
                chars[w[:testleng]] = chars.get(w[:testleng], 0)+1
            likely = sorted([(-1*chars[c], c) for c in chars])

            if len([w for w in words[:testsize] 
                if w.startswith(likely[0][1])]) == testsize: break

            words = [w for w in words if w[:testleng]==likely[0][1]]
            if len(words) < testleng: words = []

        if words and words[0] > dictwords[-1][0]:
            dictwords.append((words[0], pg0+pg_extra))
            print(pg0+pg_extra, words[0])

dictfile = 'dict.txt'
with open(dictfile, 'w') as fout:
    fout.write('\n'.join([f'{w[0]} {w[1]}' for w in dictwords]))
